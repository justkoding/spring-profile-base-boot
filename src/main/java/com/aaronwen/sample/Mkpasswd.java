package com.aaronwen.sample;

import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.Random;
import java.lang.StringBuffer;
import java.util.Set;
import java.util.HashSet;

public class Mkpasswd{
	private static final String UPPER_LETTERS = "ABCDEFGHJKMNPQRSTUVWXYZ";
	private static final String LOWER_LETTERS = "abcdefghjkmnpqrstuvwxyz";
	private static final String DIGIT = "123456789";
	private static final String SYMBOLS = "!@#$%^&*";

	public static Set<String> generate(int length){

		//3-4 upper 3-4 lower 3-4 digital 2-3 symbol
		boolean flag = false;
		String passwd = "";
		Set<String> passwds = new HashSet<String>();
		while (passwds.size() < 5){
			passwd = generatePass(length);
			flag = validatePass(passwd);
			if (flag){
				passwds.add(passwd);
			}
		}
		//passwds.forEach(System.out::println);
		return passwds;
	}

	static boolean validatePass(final String passwd){
		boolean result = false, first_flag = false, upper_flag = false,
		lower_flag = false, digit_flag = false, symbol_flag = false;
		int upper_count = 0, lower_count = 0, digit_count = 0, symbol_count = 0;
		int length = passwd.length();
		for (int i = 1; i < length; i++){
			if (UPPER_LETTERS.contains(String.valueOf(passwd.charAt(i)))){
				++upper_count;
			}
			if (LOWER_LETTERS.contains(String.valueOf(passwd.charAt(i)))){
				++lower_count;
			}
			if (DIGIT.contains(String.valueOf(passwd.charAt(i)))){
				++digit_count;
			}
			if (SYMBOLS.contains(String.valueOf(passwd.charAt(i)))){
				++symbol_count;
			}
		}
		if (upper_count == 3 || upper_count == 4){
			upper_flag = true;
		}
		if (lower_count == 3 || lower_count == 4){
			lower_flag = true;
		}
		if (digit_count == 3 || digit_count == 4){
			digit_flag = true;
		}
		if (symbol_count == 2 || symbol_count == 3){
			symbol_flag = true;
		}
		if (UPPER_LETTERS.contains(String.valueOf(passwd.charAt(0))) ||
			LOWER_LETTERS.contains(String.valueOf(passwd.charAt(0)))){
			first_flag = true;
		}

		if (first_flag && upper_flag && lower_flag && digit_flag && symbol_flag){
			return true;
		}
		return false;
	}

	static String generatePass(final int length){
		int rest_length = length;

		StringBuffer result = new StringBuffer();

		StringBuffer buf = new StringBuffer();
		buf.append(UPPER_LETTERS).append(LOWER_LETTERS);

		Random random = new Random();
		int first = random.nextInt(buf.length());
		result.append(buf.charAt(first));
		rest_length--;

		buf.deleteCharAt(first);

		buf.append(DIGIT).append(SYMBOLS);

		while (rest_length > 0){
			int index = random.nextInt(buf.length());
			result.append(buf.charAt(index));
			rest_length--;
			buf.deleteCharAt(index);
		}
		return result.toString();
	}
}