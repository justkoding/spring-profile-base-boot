package com.aaronwen.sample;

import java.util.Arrays;
import java.util.Set;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@SpringBootApplication
public class Application{
    @RequestMapping("/")
    public String index(){
        return "Hello Spring Boot";
    }

    @RequestMapping(value = "/passgen", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody HttpEntity generatePass(){
        Set<String> passwds = Mkpasswd.generate(12);
        return ResponseEntity.ok(passwds);
    }

    public static void main(String[] args){
        ApplicationContext ctx = 
            SpringApplication.run(Application.class, args);

        System.out.println("Let's inspect the beans provided by Spring Boot:");

        String[] beanNames = ctx.getBeanDefinitionNames();
        Arrays.sort(beanNames);
        for (String beanName: beanNames){
            System.out.println(beanName);
        }
    }
}
